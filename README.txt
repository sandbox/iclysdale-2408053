The solr_best_bets module provides an interface to flag search content as a "best bet" for search results, but leaves deploying the generated elevate.xml file (used to tell the search server what the bets are) as an exercise for the developer.

This module integrates with search_api_solr only, and uses the excludeIds and elevateIds query parameters to pass best bet data to the search server directly, without needing to handle elevate.xml files.

This functionality is only available with SOLR 4.7.0 and higher.
